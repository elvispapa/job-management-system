public enum JobPriority {
	VERY_LOW,
	LOW,
	MEDIUM,
	HIGH,
	VERY_HIGH
}
