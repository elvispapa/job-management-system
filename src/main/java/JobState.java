public enum JobState {
	QUEUED,
	RUNNING,
	SUCCESS,
	FAILED
}
