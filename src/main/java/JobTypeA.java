public class JobTypeA extends BaseJob implements Job {

	public JobTypeA(JobPriority priority, long id) {
		super(priority, id);
	}

	@Override
	public void performAction() {
		try {
			setState(JobState.RUNNING);

			System.out.println("Job " + getId() + " is running....");

			setState(JobState.SUCCESS);

		} catch (Exception e) {
			setState(JobState.FAILED);
		}
	}
}
