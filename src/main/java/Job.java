import java.util.Calendar;

public interface Job {
	void performAction();
	JobState getState();
	void setState(JobState state);
	Calendar getScheduledTime();
	void setScheduledTime(Calendar scheduledTime);

}
