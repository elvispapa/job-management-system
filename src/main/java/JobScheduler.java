import java.util.Calendar;
import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class JobScheduler {

	private static final int QUEUE_CAPACITY = 10;
	private final Object lock = new Object();
	private Boolean isRunning = true;

	private BlockingQueue<Job> jobsToBeExecuted = new PriorityBlockingQueue(QUEUE_CAPACITY,
			// TODO: modify the Comparator to take into the account the priority of the job
			(Comparator<Job>) (job1, job2) -> {
				Calendar job1ScheduledTime = job1.getScheduledTime();
				Calendar job2ScheduledTime = job2.getScheduledTime();

				return job1ScheduledTime.compareTo(job2ScheduledTime);
			});

	public void add(Job job, Calendar scheduledTime) {
		job.setScheduledTime(scheduledTime);
		job.setState(JobState.QUEUED);
		synchronized (lock) {
			jobsToBeExecuted.offer(job);
			lock.notify();
		}
	}

	public void start() throws InterruptedException {
		while (isRunning) {
			Job job = jobsToBeExecuted.take();
			if (job != null) {
				job.performAction();
			}

			// wait for next job to be added
			synchronized (lock) {
				job = jobsToBeExecuted.peek();
				while (job == null || jobShouldRunNow(job)) {
					if (job == null) {
						lock.wait();
					} else {
						lock.wait(jobWillRunFromNow(job));
					}
					job = jobsToBeExecuted.peek();

					System.out.println("JobScheduler is waiting for job be added.....");
				}
			}
		}
	}

	private boolean jobShouldRunNow(Job job) {
		return jobWillRunFromNow(job) <= 0;
	}

	private long jobWillRunFromNow(Job job) {
		return job.getScheduledTime().getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
	}

	public void stop() {
		this.isRunning = false;
	}
}


