import java.util.Calendar;

public class BaseJob {

	private JobState state;
	private JobPriority priority;
	private Calendar scheduledTime;
	private long id;

	public BaseJob(JobPriority priority, long id) {
		this.state = JobState.SUCCESS;
		this.priority = priority;
		this.id = id;
	}

	public JobState getState() {
		return state;
	}

	public void setState(JobState state) {
		this.state = state;
	}

	public JobPriority getPriority() {
		return priority;
	}

	public Calendar getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Calendar scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public void setPriority(JobPriority priority) {
		this.priority = priority;
	}

	public Long getId(){
		return this.id;
	}

}
