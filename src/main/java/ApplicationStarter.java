import java.util.Calendar;

public class ApplicationStarter {

	public static void main(String[] args) {
		System.out.println("ApplicationStarter is up and running");

		final JobScheduler jobScheduler = new JobScheduler();

		Calendar scheduledTimeA = Calendar.getInstance();
		scheduledTimeA.setTimeInMillis(scheduledTimeA.getTimeInMillis() + 90);

		Calendar scheduledTimeB = Calendar.getInstance();
		scheduledTimeB.setTimeInMillis(scheduledTimeB.getTimeInMillis() + 400);

		Calendar scheduledTimeC = Calendar.getInstance();
		scheduledTimeC.setTimeInMillis(scheduledTimeC.getTimeInMillis() + 500);

		jobScheduler.add(new JobTypeA(JobPriority.MEDIUM, 1), scheduledTimeA);
		jobScheduler.add(new JobTypeA(JobPriority.HIGH, 2), scheduledTimeB);
		jobScheduler.add(new JobTypeA(JobPriority.HIGH, 3), scheduledTimeC);

		// Start the jobScheduler
		new Thread(() -> {
			try {
				jobScheduler.start();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();

		// Add some jobs while the jobScheduler is running...
		new Thread(() -> {
			jobScheduler.add(new JobTypeA(JobPriority.MEDIUM, 4), scheduledTimeA);
			jobScheduler.add(new JobTypeA(JobPriority.MEDIUM, 5), scheduledTimeB);
		}).start();

	}
}
